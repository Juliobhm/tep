//
//  TEP.swift
//  ICUcalculator
//
//  Created by Julio Barado Hualde on 19/11/2018.
//  Copyright © 2018 Julio Barado Hualde. All rights reserved.
//

import UIKit

class TEP: UIViewController {

    @IBOutlet var pesiView: UIView!
    var tapGesture = UITapGestureRecognizer()
    
 


    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        
   /*
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tocarPesi")
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        pesiView.isUserInteractionEnabled = true
        pesiView.addGestureRecognizer(tap)
    }
    func tocarPesi() {
  performSegue(withIdentifier: "abrirPesi", sender: self)
        //self.pesiView.endEditing(true)
    }
 */

    }
    @IBAction func pruebaButton(_ sender: Any) {
        performSegue(withIdentifier: "abrirPesi", sender: self)
}
    
    @IBAction func abrirPesiReducido(_ sender: Any) {
        performSegue(withIdentifier: "abrirPesiReducido", sender: self)
    
}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirPesiReducido"{
            guard let transfVC = segue.destination as? pesiController else {return}
            transfVC.marcadorTipoPesi = 0
       
        }
}
}
